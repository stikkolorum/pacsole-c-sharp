﻿using System;

namespace PacSoleDEMO
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.CursorVisible = false;
            Level level1 = new Level();
            level1.Draw();
            Player player1 = new Player("Dave", 10, 3);
            player1.Draw();

            ConsoleKey key = new ConsoleKey();
            while(key!=ConsoleKey.Escape)
            {
                key = Console.ReadKey().Key;
                switch(key)
                {
                    case ConsoleKey.RightArrow:
                        player1.Move(1, 0);
                        break;
                    case ConsoleKey.LeftArrow:
                        player1.Move(-1, 0);
                        break;
                    case ConsoleKey.UpArrow:
                        player1.Move(0, -1);
                        break;
                    case ConsoleKey.DownArrow:
                        player1.Move(0,1);
                        break;
                }
            }
        }
    }
}
